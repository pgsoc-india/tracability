import hashlib
import os
import uuid
from functools import partial

from django.db import models 
from django.utils.translation import gettext_lazy as _
from django.urls import reverse

from django.utils import timezone

class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()


class AbstractModel(models.Model):

    created_at = models.DateTimeField(_("Created At"),default=timezone.now)
    updated_at = AutoDateTimeField(_("Updated At"),default=timezone.now)
    uuid = models.UUIDField(_("UUID"), primary_key=True, default=uuid.uuid4)

    class Meta:
        verbose_name = _("AbstractModel")
        verbose_name_plural = _("AbstractModels")
        abstract = True

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("AbstractModel_detail", kwargs={"pk": self.pk})
