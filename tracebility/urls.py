"""tracebility URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings

from django.contrib import admin
from django.conf.urls.static import static
from django.views.static import serve

from django.urls import path, include, re_path
from pgsoc.views import lgtracer, potracer
from pgsoc.views import onboard_fc, dashboard, onboard_lg
from pgsoc.views import view_lg, add_lg_farmer, add_lg_product, product_lg_details, add_lg_product_to_certification
from pgsoc.views import add_po_product, product_po_details, view_po, add_po_product_to_certification, onboard_po

from user.views import login_view, logout_view 

urlpatterns = [
    path('admin/', admin.site.urls),
    
    re_path('login/',login_view,name="login"),
    re_path('logout/',logout_view,name="logout"),
    
    path('',dashboard,name="dashboard"),
    path('dashboard/',dashboard,name="dashboard"),
    path('onboard/fc/',onboard_fc,name="onboard_fc"),

    path('onboard/lg/<uuid:fc_code>/',onboard_lg,name="onboard_lg"),
    path('lg/<uuid:lg_code>/',view_lg,name="view_lg"),
    path('lg/<uuid:lg_code>/add_farmer/',add_lg_farmer,name="add_lg_farmer"),
    path('lg/<uuid:lg_code>/add_product/',add_lg_product,name="add_lg_product"),
    path('lg/<str:lg_certification_code>/<uuid:product_code>/add_lg_product_to_certification/',add_lg_product_to_certification,name="add_lg_product_to_certification"),
    path('lg/<str:lg_certification_code>/',product_lg_details, name ="product_lg_details"),

    path('onboard/po/<uuid:fc_code>/',onboard_po,name="onboard_po"),
    path('po/<uuid:po_code>/add_product/',add_po_product,name="add_po_product"),    
    path('po/<uuid:po_code>/',view_po,name="view_po"),
    path('po/<str:po_certification_code>/<uuid:product_code>/add_po_product_to_certification/',add_po_product_to_certification,name="add_po_product_to_certification"),
    path('po/<str:po_certification_code>/',product_po_details, name ="product_po_details"),
    
    re_path('trace/lg/(?P<certification_id>[\w.@+-]+)/$',lgtracer),
    re_path('trace/po/(?P<certification_id>[\w.@+-]+)/$',potracer),
    
    re_path(r'^media/(?P<path>.*)$', serve, kwargs={'document_root': settings.MEDIA_ROOT}),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
