from django.apps import AppConfig


class PgsocConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pgsoc'
