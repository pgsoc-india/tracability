# Generated by Django 4.2.3 on 2023-07-27 08:04

import common.models
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('pgsoc', '0013_pocertification_approved_by_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='GenericFiles',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', common.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='UUID')),
                ('upload', models.FileField(upload_to='', verbose_name='The file referrred to')),
            ],
            options={
                'verbose_name': 'AbstractModel',
                'verbose_name_plural': 'AbstractModels',
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='facilitationcouncil',
            name='annual_audit',
            field=models.ManyToManyField(related_name='annual_audit', to='pgsoc.genericfiles', verbose_name='Annual Audit Reports'),
        ),
        migrations.AddField(
            model_name='facilitationcouncil',
            name='recommendation_letters',
            field=models.ManyToManyField(related_name='recommendation_letter', to='pgsoc.genericfiles', verbose_name='Recommendation Letter'),
        ),
        migrations.AddField(
            model_name='facilitationcouncil',
            name='registration_certificate',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='registration_certificate', to='pgsoc.genericfiles', verbose_name=''),
        ),
        migrations.AddField(
            model_name='facilitationcouncil',
            name='representation_letter',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='representation_letter', to='pgsoc.genericfiles', verbose_name=''),
        ),
    ]
