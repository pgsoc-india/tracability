# Generated by Django 4.2.3 on 2023-07-22 18:10

import common.models
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import phonenumber_field.modelfields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FacilitationCouncil',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', common.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='UUID')),
                ('name', models.CharField(max_length=500, verbose_name='FC Name')),
                ('membership_type', models.CharField(choices=[('Institutional', 'Institutional Member'), ('Associate', 'Associate Member')], default='Associate', max_length=100, verbose_name='Membership type')),
                ('FC_code', models.CharField(max_length=100, verbose_name='FC Code')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Description about the FC')),
                ('agri_notes', models.TextField(blank=True, null=True, verbose_name='Internal notes about ecology, climate etc  ')),
                ('is_approved', models.BooleanField(default=False, verbose_name='Is secretariat approved')),
                ('approved_on', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Approved on')),
                ('contact_email', models.EmailField(default='hello@example.com', max_length=254, verbose_name='Contact Email')),
                ('website', models.URLField(default='example.com', verbose_name='Website')),
                ('postal_address', models.TextField(blank=True, null=True, verbose_name='Postal Address')),
                ('organization_type', models.CharField(choices=[('society', 'Society'), ('trust', 'Trust'), ('company', 'Company')], default='company', max_length=50, verbose_name='Organization Type')),
                ('registration_number', models.TextField(default='Not given', verbose_name='Registration ')),
                ('date_of_induction', models.DateField(blank=True, null=True, verbose_name='Date of Induction')),
            ],
            options={
                'verbose_name': 'Facilitation Council',
                'verbose_name_plural': 'Facilitation Councils',
            },
        ),
        migrations.CreateModel(
            name='Farmer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500, verbose_name='Name')),
                ('contact', phonenumber_field.modelfields.PhoneNumberField(max_length=128, region=None, verbose_name='Contact Number (without +91)')),
            ],
        ),
        migrations.CreateModel(
            name='LGCertification',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('certification_id', models.CharField(max_length=100, verbose_name='Certification ID')),
                ('certification_start_date', models.DateField(verbose_name='Start Date for certification')),
                ('certification_end_date', models.DateField(verbose_name='End Date for certification')),
                ('qr_code', models.ImageField(blank=True, null=True, upload_to='qrcodes/')),
            ],
            options={
                'verbose_name': 'LGCertification',
                'verbose_name_plural': 'LGCertifications',
            },
        ),
        migrations.CreateModel(
            name='LocalGroup',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', common.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='UUID')),
                ('name', models.CharField(max_length=500, verbose_name='LG Name')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Description about the LG')),
                ('LG_code', models.CharField(max_length=200, verbose_name='LG Group Code')),
                ('no_of_farmers', models.PositiveIntegerField(verbose_name='No of Farmers')),
                ('notes', models.TextField(blank=True, null=True, verbose_name='Internal notes')),
                ('village', models.CharField(max_length=200, null=True, verbose_name='Village')),
                ('district', models.CharField(max_length=200, null=True, verbose_name='District')),
                ('state', models.CharField(max_length=100, null=True, verbose_name='State')),
                ('agro_ecological_zone', models.CharField(blank=True, max_length=50, null=True, verbose_name='Agro Ecological Zones')),
                ('agro_climatic_zone', models.CharField(blank=True, max_length=50, null=True, verbose_name='Agro Climatic Zone')),
            ],
            options={
                'verbose_name': 'LocalGroup',
                'verbose_name_plural': 'LocalGroups',
            },
        ),
        migrations.CreateModel(
            name='ParticipatingOrganization',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', common.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='UUID')),
                ('name', models.CharField(max_length=500, verbose_name='PO Name')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Description about the PO')),
                ('org_type', models.CharField(choices=[('coop', 'Co-Operative'), ('FPO', 'Farmer Producer Organization'), ('SHG', 'Self Help Group'), ('PG', 'Producer Group')], max_length=50, verbose_name='Type of Organization')),
                ('agri_notes', models.TextField(blank=True, null=True, verbose_name='Internal notes about PO. Not visible to anybody but PGS seceratariat')),
                ('fssai_license_no', models.CharField(blank=True, max_length=200, null=True, verbose_name='FSSAI License number')),
                ('endorsement_status', models.CharField(choices=[('certificed', 'Certified'), ('cancelled', 'Cancelled'), ('progress', 'InProgress')], max_length=200, verbose_name='Endorsement or Certification Status')),
                ('location', models.CharField(default='Unknown', max_length=300, verbose_name='Location of PO')),
                ('fc', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pgsoc.facilitationcouncil', verbose_name='FC associated to')),
            ],
            options={
                'verbose_name': 'Participating Organization',
                'verbose_name_plural': 'Participating Organizations',
            },
        ),
        migrations.CreateModel(
            name='ProductType',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', common.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='UUID')),
                ('name', models.CharField(max_length=500, verbose_name='Product Type')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Details about this product type')),
            ],
            options={
                'verbose_name': 'ProductType',
                'verbose_name_plural': 'ProductTypes',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', common.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='UUID')),
                ('name', models.CharField(max_length=500, verbose_name='Product Name')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Details about this product')),
                ('product_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pgsoc.producttype', verbose_name='Product Type')),
            ],
            options={
                'verbose_name': 'Product',
                'verbose_name_plural': 'Products',
            },
        ),
        migrations.CreateModel(
            name='POCertification',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('certification_id', models.CharField(max_length=100, verbose_name='Certification ID')),
                ('certification_start_date', models.DateField(verbose_name='Start Date for certification')),
                ('certification_end_date', models.DateField(verbose_name='End Date for certification')),
                ('qr_code', models.ImageField(blank=True, null=True, upload_to='qrcodes/')),
                ('fc_request', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pgsoc.facilitationcouncil', verbose_name='Requested by FC ')),
                ('po', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pgsoc.participatingorganization', verbose_name='PO certifying for')),
                ('products', models.ManyToManyField(to='pgsoc.product', verbose_name='What products are certified')),
            ],
            options={
                'verbose_name': 'POCertification',
                'verbose_name_plural': 'POCertifications',
            },
        ),
    ]
