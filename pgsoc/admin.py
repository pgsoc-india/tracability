import datetime 
from django.contrib import admin, messages
from django.utils.html import format_html
from django.http import HttpResponseRedirect

from django.urls import reverse

from pgsoc.models import FacilitationCouncil, LocalGroup, ParticipatingOrganization, Product, ProductType
from pgsoc.models import LGCertification, POCertification
from pgsoc.functions import make_qr_code



@admin.register(FacilitationCouncil)
class FacilitationCouncilAdmin(admin.ModelAdmin):

    list_display = ('uuid','FC_code','name','membership_type',"registration_number","state")
    search_fields = ('name',)
    list_filter = ('membership_type',"state","organization_type")
    
    change_form_template = "admin/fc_change.html"


    @admin.action(description='Approve FC')
    def approve_fc(self, request, queryset):
        for obj in queryset:
            if not obj.is_approved:
                obj.is_approved = True
                obj.date_of_induction = datetime.datetime.now()
                obj.approved_on = datetime.datetime.now()
                obj.approved_by = request.user 
                obj.FC_code = FacilitationCouncil.objects.count() + 1
                obj.save()
                self.message_user(
                    request,"FC successfully approved", messages.SUCCESS
                )
    
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["uuid","FC_code","name","membership_type","registration_number","state","country","approved_by","approved_on","is_approved","date_of_induction","organization_type","city","postal_address","website","contact_email","poc","legal_representative","created_at","updated_at","bylaws","registration_certificate"]
        else:
            return self.readonly_fields
    
    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return True
        

    

@admin.register(ParticipatingOrganization)
class ParticipatingOrganizationAdmin(admin.ModelAdmin):
    
    list_display = ('name','org_type','fc','fssai_license_no','endorsement_status')
    search_fields = ('name','description')
    list_filter = ('org_type','endorsement_status')
    
        
    change_form_template = "admin/po_changeform.html"

    def response_change(self, request, obj):
        if request.POST:
            # do whatever you want the button to do
            obj.is_approved = True
            obj.approved_on = datetime.datetime.now()
            obj.approved_by = request.user 
            if "approve_po_low_risk" in request.POST:
                obj.risk_category = "low"
            elif "approve_po_low_risk" in request.POST:
                obj.risk_category = "moderate"
            else:
                obj.risk_category = "high"
            obj.po_code = ParticipatingOrganization.objects.count() + 1
            obj.endorsement_status = "certified"
            obj.save()
            self.message_user(
                request,"Certification successfully issued", messages.SUCCESS
            )
            return HttpResponseRedirect(".")  # stay on the same detail page
        return super().response_change(request, obj)
    
    # def get_readonly_fields(self, request, obj=None):
    #     if 'edit' not in request.GET:
    #         return ["certification_id","fc_request","lg","products","certification_start_date","certification_end_date","is_approved","approved_on","approved_by","qr_code"]
    #     else:
    #         return self.readonly_fields

@admin.register(ProductType)
class ProductTypeAdmin(admin.ModelAdmin):
    list_display = ("name","description")
    search_fields = ("name",)

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ("name","product_type","description")
    search_fields = ("name",)
    list_filter = ("product_type",)

@admin.register(LocalGroup)
class LocalGroupAdmin(admin.ModelAdmin):
    list_display = ('name',"LG_code","no_of_farmers","agro_ecological_zone","agro_climatic_zone")
    search_fields = ("name","LG_code")
    list_filter = ("agro_ecological_zone","agro_climatic_zone")    

@admin.register(LGCertification)
class LGCertificationAdmin(admin.ModelAdmin):
    list_display = ('id','certification_id','fc_request','lg','certification_start_date','certification_end_date','get_qr_link')
    list_filter = ('fc_request',)
    search_fields = ('certification_id',)
    
    
    change_form_template = "admin/lgcertification_changeform.html"

    def response_change(self, request, obj):
        if "approve_product_certification" in request.POST:
            # do whatever you want the button to do
            obj.is_approved = True
            obj.certification_start_date = datetime.datetime.now()
            three_mon_rel = datetime.timedelta(days=3*365)
            obj.certification_end_date = datetime.datetime.now() + three_mon_rel
            obj.approved_by = request.user 
            obj.approved_on = datetime.datetime.now()
            count = LGCertification.objects.all().count()+1
            obj.certification_id = obj.lg.state[:2].upper()+"-"+obj.lg.LG_code+"-"+str(datetime.datetime.now().year)+"-"+str(count)
            path = make_qr_code(obj, "https://connect.pgsorganic.in"+reverse('product_lg_details', kwargs={"lg_certification_code":obj.certification_id}),c_for="lg")
            obj.qr_code = path
            obj.save()
            self.message_user(
                request,"Certification successfully issued", messages.SUCCESS
            )
            return HttpResponseRedirect(".")  # stay on the same detail page
        return super().response_change(request, obj)
    
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["certification_id","fc_request","lg","products","certification_start_date","certification_end_date","is_approved","approved_on","approved_by","qr_code"]
        else:
            return self.readonly_fields
    

    def get_qr_link(self, obj):
        name = "QR Code"
        return format_html("<a href='{url}'>{url}</a>", url="/trace/lg/"+obj.certification_id)

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return True
        
    
@admin.register(POCertification)
class POCertificationAdmin(admin.ModelAdmin):
    list_display = ('certification_id','po','certification_start_date','certification_end_date','get_qr_link')
    # list_filter = ('fc_request',)
    search_fields = ('certification_id',)
    
    def get_qr_link(self, obj):
        name = "QR Code"
        return format_html("<a href='{url}'>{url}</a>", url=reverse('product_po_details', kwargs={"po_certification_code":obj.certification_id}))

    
    change_form_template = "admin/pocertification_changeform.html"

    def response_change(self, request, obj):
        if "approve_product_certification" in request.POST:
            # do whatever you want the button to do
            obj.is_approved = True
            obj.certification_start_date = datetime.datetime.now()
            three_mon_rel = datetime.timedelta(days=1*365)
            if "low" in obj.po.risk_category:
                three_mon_rel = datetime.timedelta(days=3*365)
            obj.certification_end_date = datetime.datetime.now() + three_mon_rel
            obj.approved_by = request.user 
            count = POCertification.objects.all().count()+1
            import ipdb;ipdb.set_trace()
            obj.certification_id = str(obj.po.fc.pk)+"-"+str(obj.po.pk)+"-2023-"+str(count),
            
            # obj.certification_id = obj.lg.state[:2].upper()+"-"+obj.lg.LG_code+"-"+str(datetime.datetime.now().year)+"-"+str(count)
            path = make_qr_code(obj, "https://connect.pgsorganic.in"+reverse('product_po_details', kwargs={"po_certification_code":obj.certification_id}),c_for="po")
            obj.qr_code = path
            obj.save()
            self.message_user(
                request,"Certification successfully issued", messages.SUCCESS
            )
            return HttpResponseRedirect(".")  # stay on the same detail page
        return super().response_change(request, obj)
    
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["certification_id","po","products","certification_start_date","certification_end_date","is_approved","approved_on","approved_by","qr_code"]
        else:
            return self.readonly_fields
    

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return False
    
        

    
# admin.site.disable_action("delete_selected")