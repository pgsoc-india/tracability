from django.http import Http404
from django.shortcuts import render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponseNotFound, HttpResponse
from django.shortcuts import get_list_or_404, get_object_or_404
from django.utils.timezone import localtime, now
from django.urls import reverse

from cities_light.models import Region, City, Country
from pgsoc.models import FacilitationCouncil
from pgsoc.models import LGCertification, LocalGroup
from pgsoc.models import ParticipatingOrganization, POCertification
from pgsoc.models import ProductType, Product
from pgsoc.models import GenericFiles
from pgsoc.models import Farmer 

from user.models import User 



def lgtracer(request,certification_id):
    try:
        lgc = LGCertification.objects.get(certification_id=certification_id)
    except LGCertification.DoesNotExist:
        raise Http404("The certification ID is wrong. Please re-check the certification ID")
    return render(request, 'lgtracer.html',{'lgc':lgc})

def potracer(request,certification_id):
    try:
        poc = POCertification.objects.get(certification_id=certification_id)
    except POCertification.DoesNotExist:
        raise Http404("The certification ID is wrong. Please re-check the certification ID")
    return render(request, 'potracer.html',{'poc':poc})


def filestore(fileobj):
    myfile = GenericFiles.objects.create(upload=fileobj,orig_name=fileobj.name,file_extension=fileobj.content_type, orig_size=fileobj.size)
    return myfile

def onboard_fc(request):
    if request.method == "POST":
        data = request.POST
        files = request.FILES 
        
        organization_name = data.get("organization_name")
        organization_reg_no = data.get("organization_reg_no")
        org_type = data.get("org_type")
        postal_address = data.get("postal_address")
        location = data.get("location")
        city = City.objects.get(id = location )
        country = city.country
        state = city.region
        registration_document = filestore(files.get("registration_document"))
        recommendation_letter = files.getlist("recommendation_letter")
        annual_audit = files.getlist("annual_audit")
        bylaws = filestore(files.get("bylaws"))
        
        
        website = data.get("website")
        contact_email = data.get("contact_email")
        
        ## Legal User creation
        legal_name = data.get("legal_name")
        legal_contact = data.get("legal_contact")
        legal_email = data.get("legal_email")
        if not User.objects.filter(username = legal_email).exists():
            legal_user = User.objects.create(first_name=legal_name,email=legal_email,contact_number=legal_contact,username = legal_email)
        else:
            legal_user = User.objects.get(username = legal_email)

        ## User onboarding
        user_email = data.get("user_email")
        user_contact = data.get("user_contact")
        user_name = data.get("user_name")
        password = data.get("user_password")
        user = None
        if not User.objects.filter(username = user_email).exists():
            user = User.objects.create(first_name=user_name,email=user_email,contact_number=user_contact,username=user_email)
            user.set_password(password)
            user.save()
        if (user_email == legal_email):
            if legal_user.password is None:
                legal_user.set_password(password)
                legal_user.save()
            user = User.objects.get(username = legal_email)
        else:
            user = User.objects.get(username = user_email)
        if not FacilitationCouncil.objects.filter(registration_number = organization_reg_no).exists():
            fc = FacilitationCouncil.objects.create(
                name = organization_name,
                website= website,
                organization_type = org_type,
                registration_number = organization_reg_no,
                contact_email = contact_email,
                legal_representative = legal_user,
                membership_type = "Institutional",
                country = country,
                state = state,
                city = city,
                postal_address = postal_address,
                bylaws = bylaws,
                registration_certificate=registration_document
            )
            fc.poc.add(user)
            fc.save()
        
        
        return render(request,'onboard_fc_data_received.html')
    else:
        states = Region.objects.all()
        cities = City.objects.all()
        return render(request,'onboard_fc.html',{'states':states,'cities':cities})

def onboard_po(request,fc_code):
    
    if request.method == "POST":
        fc = FacilitationCouncil.objects.get(uuid=fc_code)
        data = request.POST
        
        organization_name = data.get("organization_name")
        organization_reg_no = data.get("organization_reg_no")
        org_type = data.get("org_type")
        postal_address = data.get("postal_address")
        location = data.get("location")
        city = City.objects.get(id = location )
        country = city.country
        state = city.region
        
        
        website = data.get("website")
        contact_email = data.get("contact_email")
        
        ## User onboarding
        user_email = data.get("user_email")
        user_contact = data.get("user_contact")
        user_name = data.get("user_name")
        password = data.get("user_password")
        if not User.objects.filter(username = user_email).exists():
            user = User.objects.create(first_name=user_name,email=user_email,contact_number=user_contact,username=user_email)
            user.set_password(password)
            user.save()
        else:
            user = User.objects.get(username = user_email)
        if not ParticipatingOrganization.objects.filter(registration_number = organization_reg_no).exists():
            fc = ParticipatingOrganization.objects.create(
                name = organization_name,
                website= website,
                org_type = org_type,
                registration_number = organization_reg_no,
                contact_email = contact_email,
                country = country,
                state = state,
                city = city,
                address = postal_address,
                fc=fc,
            )
            fc.poc.add(user)
            fc.save()
        return HttpResponseRedirect(reverse("dashboard"))
    else:
        states = Region.objects.all()
        cities = City.objects.all()
        return render(request,'onboard_po.html',{'states':states,'cities':cities})

    
    
@login_required
def dashboard(request):
    user = request.user
    fc = FacilitationCouncil.objects.filter(poc__in=[user]).first()
    po = ParticipatingOrganization.objects.filter(poc__in=[user]).first()
    if fc:
        fc_lg = LocalGroup.objects.filter(fc_associated=fc)
        certified_products = LGCertification.objects.filter(fc_request=fc)
        po = ParticipatingOrganization.objects.filter(fc=fc)
        pending_approvals = POCertification.objects.filter(po__fc=fc)
        return render(request,'dashboard.html',{'fc':fc,'fc_lg':fc_lg,"certified_products":certified_products,'po':po,})
    elif po:
        return HttpResponseRedirect(reverse("view_po",kwargs={"po_code":po.uuid}))
    else:
        return HttpResponse("Invalid login")

@login_required
def onboard_lg(request,fc_code):
    fc = FacilitationCouncil.objects.get(uuid=fc_code)
    if request.method == "POST":
        data = request.POST
        lg_name = data.get("lg_name")
        village = data.get("village")
        district = data.get("district")
        state = data.get("state")
        
        convener_name = data.get("convener_name")
        convener_contact = data.get("convener_contact")
        farmer = None  
        if not Farmer.objects.filter(contact = convener_contact).exists():
            farmer = Farmer.objects.create(name=convener_name,contact=convener_contact)
        else:
            farmer = Farmer.objects.get(contact = convener_contact)
            
        user = request.user
        if user in fc.poc.all():      
            lg = LocalGroup.objects.create(
                name = lg_name, 
                village=village, 
                district=district, 
                state = state, 
                no_of_farmers = 1, 
                fc_associated = fc
                )
            lg.LG_code = LocalGroup.objects.count() + 1
            lg.convener = farmer 
            lg.save()
        
        return HttpResponseRedirect("/dashboard/")
    else:
        return render(request,"onboard_lg.html",{"fc":fc})
    
@login_required
def view_lg(request,lg_code):

    lg = LocalGroup.objects.get(uuid = lg_code)
    if request.method == "POST":
        if not LGCertification.objects.filter(lg=lg).first():
            lpc = LGCertification.objects.create(
                fc_request = lg.fc_associated,
                lg = lg,
                certification_start_date= localtime(now()).date(),
                certification_end_date = localtime(now()).date()
            )
            lpc.save()
            return HttpResponseRedirect(reverse('view_lg',kwargs={"lg_code":lg.uuid}))
        else:
            return HttpResponse("Already Certified")
    else:
        try:
            lg_status = LGCertification.objects.filter(lg=lg).first()
            if request.user in lg.fc_associated.poc.all():
                return render(request,'lg_details.html',{'lg':lg,'lg_status':lg_status})
        except LocalGroup.DoesNotExist:
            return HttpResponseNotFound("LG Not found")          

@login_required
def add_lg_farmer(request,lg_code):
    if request.method == "POST":
        farmer_name = request.POST.get("farmer_name")
        farmer_contact = request.POST.get("farmer_contact")
        lg = LocalGroup.objects.get(LG_code = lg_code)
        if lg and (request.user in lg.fc_associated.poc.all()):
            farmer = Farmer.objects.create(name=farmer_name,contact=farmer_contact)
            farmer.save()
            lg.associated_farmers.add(farmer)
            return HttpResponseRedirect("/view/lg/%s/"%lg_code)
        else:
            return HttpResponseNotFound("LG not found") 
    else:
        return render(request,"onboard_lg_farmer.html")
    
    
@login_required
def add_lg_product(request,lg_code):
    if request.method == "POST":
        producttype_id = request.POST.get("producttype")
        product_id = request.POST.get("product")
        product_text = request.POST.get("product_text")
        producttype = ProductType.objects.get(uuid=producttype_id)
        if product_id:
            product = Product.objects.get(uuid=product_id)
        else:
            product,status = Product.objects.get_or_create(name=product_text,product_type = producttype) 
        lg = LocalGroup.objects.get(uuid = lg_code)
        if lg and (request.user in lg.fc_associated.poc.all()):
            lg.products.add(product)
            return HttpResponseRedirect("/view/lg/%s/"%lg_code)
        else:
            return HttpResponseNotFound("LG not found") 
    else:
        product_types = ProductType.objects.filter(is_raw=True)
        products = Product.objects.filter(product_type__is_raw=True)
        return render(request,"add_products.html",{"product_types":product_types,"products":products})
    

@login_required
def add_lg_product_to_certification(request,lg_certification_code, product_code):
    lgc = get_object_or_404(LGCertification, certification_id=lg_certification_code) 
    product = get_object_or_404(Product,uuid=product_code)
    if product not in lgc.products.all():
        lgc.products.add(product)
    return HttpResponseRedirect(reverse("view_lg",kwargs={"lg_code":lgc.lg.uuid}))


def product_lg_details(request, lg_certification_code):
    lgc = get_object_or_404(LGCertification,certification_id=lg_certification_code)
    return render(request,'lgtracer.html',{"lgc":lgc})

@login_required
def view_po(request, po_code):
    po = get_object_or_404(ParticipatingOrganization, uuid=po_code)
    
    if request.method == "POST":        
        poc = POCertification.objects.create(
            po = po,
            certification_start_date= localtime(now()).date(),
            certification_end_date = localtime(now()).date()
        )
        poc.save()
    poc = POCertification.objects.filter(po=po,certification_end_date__gte = (localtime(now()).date())).first()
    return render(request, 'view_po.html',{"po":po,"po_status":poc})

@login_required
def add_po_product(request,po_code):
    po = get_object_or_404(ParticipatingOrganization, uuid=po_code)
    if request.method == "POST":
        product_id = request.POST.get("product")
        product_text = request.POST.get("product_text")
        producttype = ProductType.objects.get(name="Processed Food")
        if product_id:
            product = Product.objects.get(uuid=product_id)
        else:
            product, status = Product.objects.get_or_create(name=product_text,product_type = producttype) 
        if po and (request.user in po.poc.all()):
            po.products.add(product)
            return HttpResponseRedirect(reverse('dashboard'))
        else:
            return HttpResponseNotFound("PO not found") 
    else:
        product_types = ProductType.objects.filter(is_processed=True)
        products = Product.objects.filter(product_type__is_processed=True)
        return render(request,"add_products.html",{"product_types":product_types,"products":products})
    
@login_required
def add_po_product_to_certification(request,po_certification_code, product_code):
    poc = get_object_or_404(POCertification, certification_id=po_certification_code) 
    product = get_object_or_404(Product,uuid=product_code)
    if product not in poc.products.all():
        poc.products.add(product)
    return HttpResponseRedirect(reverse("view_po",kwargs={"po_code":poc.po.uuid}))

  
def product_po_details(request, po_certification_code):
    poc =get_object_or_404(POCertification, certification_id=po_certification_code)
    return render(request,'potracer.html',{"poc":poc})
 