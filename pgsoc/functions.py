#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import qrcode
from qrcode.image.styles.moduledrawers import RoundedModuleDrawer
from qrcode.image.styles.moduledrawers import CircleModuleDrawer
from  qrcode.image.styles.colormasks import SolidFillColorMask
from qrcode.image.styledpil import StyledPilImage
from PIL import Image, ImageDraw
from django.conf import settings

def style_eyes(img):
  img_size = img.size[0]
  eye_size = 70 #default
  quiet_zone = 40 #default
  mask = Image.new('L', img.size, 0)
  draw = ImageDraw.Draw(mask)
  draw.rectangle((40, 40, 110, 110), fill=255)
  draw.rectangle((img_size-110, 40, img_size-40, 110), fill=255)
  draw.rectangle((40, img_size-110, 110, img_size-40), fill=255)
  return mask

def make_qr_code(obj, url, c_for):

    qr = qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)
    qr.add_data(url)
    qr_eyes_img = qr.make_image(image_factory=StyledPilImage,
                                eye_drawer=RoundedModuleDrawer(radius_ratio=0.9),
                                color_mask=SolidFillColorMask(back_color=(255, 255, 255), front_color=(0,128,0)))

    qr_img = qr.make_image(image_factory=StyledPilImage,
                        module_drawer=CircleModuleDrawer(),
                        color_mask=SolidFillColorMask(front_color=(0,128,0)),
                        embeded_image_path=settings.STATICFILES_DIRS[0]+"/images/pgsoc_logo.jpg")

    mask = style_eyes(qr_img)
    final_img = Image.composite(qr_eyes_img, qr_img, mask)
    file_path =  c_for+"_"+str(obj.certification_id) +".png"
    path = os.path.join("media/qrcodes/", file_path)
    final_img.save(path)
    return path
