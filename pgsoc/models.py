import datetime 
from django.db import models
from django.utils.translation import gettext as _
from django.db.models.signals import post_save
from django.db.models import signals
from django.core.files.base import ContentFile
from django.urls import reverse

from user.models import User 
from common.models import AbstractModel

from phonenumber_field.modelfields import PhoneNumberField
# from djmodels.TextField.models import models.TextField
from pgsoc.functions import make_qr_code


class GenericFiles(AbstractModel):
    upload = models.FileField(_("The file referrred to"), upload_to="", max_length=100)
    orig_name = models.CharField(_("Original Media name"), max_length=500,default="None")
    orig_size = models.PositiveIntegerField(_("File Size"), default=0)
    file_extension = models.CharField(_("File extension"), max_length=50,default="")

    class Meta:
        ordering=['-updated_at']

    def __str__(self):
        return str(self.orig_name)
    
    

class FacilitationCouncil(AbstractModel):
    
    membership_type_choices = (
        ("Institutional","Institutional Member"),
        ("Associate","Associate Member")
    )
    organization_choices = (
        ("society","Society"),
        ("trust","Trust"),
        ("company","Company")
    )
    
    agri_notes = models.TextField(_("Internal notes about ecology, climate etc  "),blank=True,null=True)
   
    legal_representative = models.ForeignKey("user.User", verbose_name=_("Legal Representative"), on_delete=models.CASCADE, related_name = "fc_legal_representative",blank=True,null=True)
    poc = models.ManyToManyField("user.User", verbose_name=_("Point of Contact"))
   
    is_approved = models.BooleanField(_("Is secretariat approved"),default=False)
    approved_on = models.DateTimeField(_("Approved on"), auto_now=False, auto_now_add=False,default=None,null=True,blank=True)
    approved_by = models.ForeignKey("user.User", verbose_name=_("Approved By"), on_delete=models.CASCADE, default=None, null=True, blank=True,related_name="approved_by_user" )
    
    name = models.CharField(_("FC Name"),max_length=500)
    membership_type = models.CharField(_("Membership type"),choices=membership_type_choices,default="Associate",max_length=100)
    FC_code = models.CharField(_("FC Code"),max_length=100)
    description = models.TextField(_("Description about the FC"),blank=True,null=True)
    website = models.URLField(_("Website"), max_length=200,blank=False,null= False, default="example.com")
    contact_email = models.EmailField(_("Contact Email"), max_length=254,blank=False,null=False,default="hello@example.com")
    postal_address = models.TextField(_("Postal Address"),blank=True, null=True)
    organization_type = models.CharField(_("Organization Type"), max_length=50,choices = organization_choices,default = "company")
    registration_number = models.TextField(_("Registration "),default="Not given")
    date_of_induction = models.DateField(_("Date of Induction"), auto_now=False, auto_now_add=False,blank=True, null=True)
    
    annual_audit = models.ManyToManyField("pgsoc.GenericFiles", verbose_name=_("Annual Audit Reports"),related_name="annual_audit")
    recommendation_letters = models.ManyToManyField("pgsoc.GenericFiles", verbose_name=_("Recommendation Letter"),related_name="recommendation_letter")
    registration_certificate = models.ForeignKey("pgsoc.GenericFiles", verbose_name=_("Registration Certificate"), on_delete=models.CASCADE,related_name="registration_certificate",default=None,null = True, editable = True,)
    representation_letter = models.ForeignKey("pgsoc.GenericFiles", verbose_name=_("Representation Letter"), on_delete=models.CASCADE,related_name="representation_letter",default=None,null = True, editable = True,)
    bylaws = models.ForeignKey("pgsoc.GenericFiles", verbose_name=_("ByLaws"), on_delete=models.CASCADE,related_name="bylaws",default=None,null = True, editable = True,)
    
    country = models.ForeignKey('cities_light.Country', on_delete=models.SET_NULL, null=True, blank=True) 
    city = models.ForeignKey('cities_light.City', on_delete=models.SET_NULL, null=True, blank=True)
    state = models.ForeignKey('cities_light.Region', on_delete=models.SET_NULL, null=True, blank=True)
    
    class Meta:
        verbose_name = _("Facilitation Council")
        verbose_name_plural = _("Facilitation Councils")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("FacilitationCouncil_detail", kwargs={"pk": self.pk})

class LocalGroup(AbstractModel):

    name = models.CharField(_("LG Name"),max_length=500)
    description = models.TextField(_("Description about the LG"),blank=True,null=True)
    LG_code = models.CharField(_("LG Group Code"), max_length=200)
    no_of_farmers = models.PositiveIntegerField(_("No of Farmers"))
    notes = models.TextField(_("Internal notes"),blank=True,null=True)
    village = models.CharField(_("Village"), max_length=200,null=True)
    district = models.CharField(_("District"), max_length=200,null=True)
    state = models.CharField(_("State"), max_length=100,null=True)
    convener = models.ForeignKey("pgsoc.Farmer", verbose_name=_("Convener"), on_delete=models.CASCADE,related_name="lg_convener",null=True)
    associated_farmers = models.ManyToManyField("pgsoc.Farmer", verbose_name=_("List of farmers"),related_name='lg_farmers')
    agro_ecological_zone = models.CharField(_("Agro Ecological Zones"), max_length=50,blank=True,null=True)
    agro_climatic_zone = models.CharField(_("Agro Climatic Zone"), max_length=50,blank=True,null=True)
    fc_associated = models.ForeignKey("pgsoc.FacilitationCouncil", verbose_name=_("FC Associated with"), on_delete=models.CASCADE,default=None, blank=True,null=True)
    products = models.ManyToManyField("pgsoc.Product", verbose_name=_("Products produced by LG"))
    
    class Meta:
        verbose_name = _("LocalGroup")
        verbose_name_plural = _("LocalGroups")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("LocalGroup_detail", kwargs={"pk": self.pk})

    
class ParticipatingOrganization(AbstractModel):
    
    org_type = (
        ("coop","Co-Operative"),
        ("FPO","Farmer Producer Organization"),
        ("SHG","Self Help Group"),
        ("PG","Producer Group"),
        ('RME','Repacker and Marketing entity')
    )
    
    endorsement_status_choices = (
        ("certificed","Certified"),
        ("cancelled","Cancelled"),
        ("documentation","Verification and Documentation in progress"),
        ("progress","InProgress")
    )
    
    risk_categories = (
        ("low","Low Risk"),
        ("moderate","Moderate Risk"),
        ("high","High Risk")
    )
    
    po_code = models.CharField(_("PO Code"), max_length=100,default="", blank=True,null=True)
    name = models.CharField(_("PO Name"),max_length=500)
    registration_number = models.TextField(_("Registration "),default="Not given")
    fssai_license_no = models.CharField(_("FSSAI License number"), max_length=200,blank=True,null=True)
    description = models.TextField(_("Description about the PO"),blank=True,null=True)
    org_type = models.CharField(_("Type of Organization"), max_length=50,choices=org_type)
    agri_notes = models.TextField(_("Internal notes about PO. Not visible to anybody but PGS seceratariat"),blank=True,null=True)
    is_independent = models.BooleanField(_("Independent PO"), default=False)
    fc = models.ForeignKey("pgsoc.FacilitationCouncil", verbose_name=_("FC associated to"), on_delete=models.CASCADE, blank=True, null=True)
    endorsement_status = models.CharField(_("Endorsement or Certification Status"), max_length=200,choices=endorsement_status_choices, default="progress")
    address = models.TextField(_("address of PO"),blank=False,null=False,default="Unknown")
    country = models.ForeignKey('cities_light.Country', on_delete=models.SET_NULL, null=True, blank=True) 
    city = models.ForeignKey('cities_light.City', on_delete=models.SET_NULL, null=True, blank=True)
    state = models.ForeignKey('cities_light.Region', on_delete=models.SET_NULL, null=True, blank=True)
    risk_category = models.CharField(_("Risk Category"), choices = risk_categories, max_length=50, default="high")
    is_approved = models.BooleanField(_("Is secretariat approved"),default=False)
    approved_on = models.DateTimeField(_("Approved on"), auto_now=False, auto_now_add=False,default=None,null=True,blank=True)
    approved_by = models.ForeignKey("user.User", verbose_name=_("Approved By"), on_delete=models.CASCADE, default=None, null=True, blank=True,related_name="po_approved_by_user" )
    contact_email = models.EmailField(_("Contact Email"), max_length=254,blank=False,null=False,default="hello@example.com")
    website = models.URLField(_("Website"), max_length=200,blank=False,null= False, default="example.com")
    poc = models.ManyToManyField("user.User", verbose_name=_("Point of Contact"))
    products = models.ManyToManyField("pgsoc.Product", verbose_name=_("Products produced by LG"), related_name = "po_products",blank=True,null=True)
    
    class Meta:
        verbose_name = _("Participating Organization")
        verbose_name_plural = _("Participating Organizations")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("ParticipatingOrganization_detail", kwargs={"pk": self.pk})


class ProductType(AbstractModel):

    name = models.CharField(_("Product Type"),max_length=500)
    description = models.TextField(_("Details about this product type"),blank=True,null=True)
    is_raw = models.BooleanField(_("Raw product type"),default=False)
    is_processed = models.BooleanField(_("Processed product type"),default=False)
  
    class Meta:
        verbose_name = _("ProductType")
        verbose_name_plural = _("ProductTypes")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("ProductType_detail", kwargs={"pk": self.pk})


class Product(AbstractModel):
    
    name = models.CharField(_("Product Name"),max_length=500)
    product_type = models.ForeignKey("pgsoc.ProductType", verbose_name=_("Product Type"), on_delete=models.CASCADE)
    description = models.TextField(_("Details about this product"),blank=True,null=True)


    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Product_detail", kwargs={"pk": self.pk})


class LGCertification(models.Model):

    certification_id = models.CharField(_("Certification ID"), max_length=100)
    fc_request = models.ForeignKey("pgsoc.FacilitationCouncil", verbose_name=_("Requested by FC "), on_delete=models.CASCADE)
    lg = models.ForeignKey("pgsoc.LocalGroup", verbose_name=_("Local Group"), on_delete=models.CASCADE)
    products = models.ManyToManyField("pgsoc.Product", verbose_name=_("Products"))
    certification_start_date = models.DateField(_("Start Date for certification"), auto_now=False, auto_now_add=False)
    certification_end_date = models.DateField(_("End Date for certification"), auto_now=False, auto_now_add=False)
    qr_code = models.ImageField(upload_to='qrcodes/',blank=True,null=True)
    is_approved = models.BooleanField(_("Is secretariat approved"),default=False)
    approved_on = models.DateTimeField(_("Approved on"), auto_now=False, auto_now_add=False,default=None,null=True,blank=True)
    approved_by = models.ForeignKey("user.User", verbose_name=_("Approved By"), on_delete=models.CASCADE, default=None, null=True, blank=True,related_name="lg_certification_approved_by")

    class Meta:
        verbose_name = _("LGCertification")
        verbose_name_plural = _("LGCertifications")

    def __str__(self):
        return self.certification_id
    

    def get_absolute_url(self):
        return reverse("LGCertification_detail", kwargs={"pk": self.pk})
    
    
class POCertification(models.Model):

    certification_id = models.CharField(_("Certification ID"), max_length=100)
    po = models.ForeignKey("pgsoc.ParticipatingOrganization", verbose_name=_("PO certifying for"), on_delete=models.CASCADE)
    products = models.ManyToManyField("pgsoc.Product", verbose_name=_("What products are certified"))
    certification_start_date = models.DateField(_("Start Date for certification"), auto_now=False, auto_now_add=False)
    certification_end_date = models.DateField(_("End Date for certification"), auto_now=False, auto_now_add=False)
    qr_code = models.ImageField(upload_to='qrcodes/',blank=True,null=True)
    is_approved = models.BooleanField(_("Is secretariat approved"),default=False)
    approved_on = models.DateTimeField(_("Approved on"), auto_now=False, auto_now_add=False,default=None,null=True,blank=True)
    approved_by = models.ForeignKey("user.User", verbose_name=_("Approved By"), on_delete=models.CASCADE, default=None, null=True, blank=True,related_name="po_certification_approved_by")

    class Meta:
        verbose_name = _("POCertification")
        verbose_name_plural = _("POCertifications")

    def __str__(self):
        return self.certification_id

    def get_absolute_url(self):
        return reverse("POCertification_detail", kwargs={"pk": self.pk})
    
    def save(self, *args, **kwargs):
        # self.certification_id = self.po.state[:2].upper()+"-"+self.lg.PO_code+"-"+str(datetime.datetime.now().year)+"-"+str(count)
    #     path = make_qr_code(self.certification_id, "https://trace.pgsorganic.in/trace/po/"+self.certification_id+"/","po")
    #     self.qr_code = path
        super(POCertification, self).save(*args, **kwargs)

class Farmer(models.Model):
    name = models.CharField(_("Name"), max_length=500)
    contact = PhoneNumberField(_("Contact Number (without +91)"))
    