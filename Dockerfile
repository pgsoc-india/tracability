FROM python:3.8
ENV PYTHONUNBUFFERED 1

# Allows docker to cache installed dependencies between builds
COPY . code
WORKDIR code

RUN pip install -r requirements/requirements-dev.txt

# Adds our application code to the image

EXPOSE 8000

# Run the production server
CMD python manage.py runserver
