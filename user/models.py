from django.db import models
from django.utils.translation import gettext as _
from django.contrib.auth.models import AbstractUser

from phonenumber_field.modelfields import PhoneNumberField


class User(AbstractUser):
    contact_number = PhoneNumberField(_("Contact Number"))
