from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseNotFound, HttpResponse
from django.utils.timezone import localtime, now
from django.urls import reverse

from django.contrib.auth import authenticate, login
from django.contrib.auth import logout




def login_view(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirect to a success page.
            return HttpResponseRedirect("/dashboard/")
        else:
            return render(request,'login.html')
    else:
        return render(request,'login.html')
    
    


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("login"))